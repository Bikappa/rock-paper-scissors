# Simple Rock Paper scissors game

## Install

    $ git clone https://bitbucket.org/Bikappa/rock-paper-scissors.git
    $ cd rock-paper-scissors
    $ npm install

  Dependencies are for building (with webpack), testing (with mocha and chai) and serving (with serve, see note below).

## Build
    $ npm run build

## Start
    $ npm start
  
  The game will be available at http://localhost:5000 .

> **_NOTE:_** the application was intended to be completely offline, but unfortunately it's not possible to load local html files with fetch requests. Hence the quick workaround using a little webserver 

## Usage
    Select the game mode and then hit the "Next round" button to play
    In Player vs CPU mode the round waits until user selection (CPU choose fast) while in CPU vs CPU it's rng (random number generator) vs rng