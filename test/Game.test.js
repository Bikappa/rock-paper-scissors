const { GameMode, Game } = require('../src/views/Game')
const Player = require('../src/player/Player')
const CPUPlayer = require('../src/player/CPUPlayer')

const expect = require('chai').expect

describe('Game', function(){
  describe('constructor player vs cpu', function(){
    it('Should create one human and one cpu player', function(){

      const game = new Game( GameMode.PLAYER_VS_CPU, undefined);
        
      expect(game.playerA).to.be.instanceOf(Player)
      expect(game.playerA).to.not.be.instanceOf(CPUPlayer)
      expect(game.playerB).to.be.instanceOf(CPUPlayer)

    })
  })
})