const GameRound = require('../src/game/Round')
const Rules = require('../src/game/Rules')
const Player = require('../src/player/Player')
const expect = require('chai').expect


function buildDummyGameRound(choicheA, choicheB){
  const playerA = new Player('Bob')
  const playerB = new Player('Alice')

  //these test players decide immediatly what they play
  playerA.askForChoice = (done) => {
    done(choicheA)
  }
  playerB.askForChoice = (done) => {
    done(choicheB)
  }
  
  return new GameRound(Rules.STRENGTH_MAP, playerA, playerB)
  
}

describe('Game set', function () {

  describe('getWinner()', function() {
    it('should return undefined if it is a tie', function(){
        const gameRound = buildDummyGameRound(Rules.CHOICES.PAPER, Rules.CHOICES.PAPER)
        gameRound.start(() => {
          expect(gameRound.getWinner()).to.be.undefined

        })
    })

    it('should make scissors win over paper', function(){
      
      const gameRound = buildDummyGameRound(Rules.CHOICES.SCISSORS, Rules.CHOICES.PAPER)

      gameRound.start(() => {
        expect(gameRound.getWinner()).to.be.equal(gameRound.playerA)
      })
    })

    it('should make paper win over rock', function(){
      
      const gameRound = buildDummyGameRound(Rules.CHOICES.ROCK, Rules.CHOICES.PAPER)

      gameRound.start(() => {
        expect(gameRound.getWinner()).to.be.equal(gameRound.playerB)
      })
    })


    it('should make rock win over scissors', function(){
      
      const gameRound = buildDummyGameRound(Rules.CHOICES.ROCK, Rules.CHOICES.SCISSORS)

      gameRound.start(() => {
        expect(gameRound.getWinner()).to.be.equal(gameRound.playerA)
      })
    })
  })
  
})