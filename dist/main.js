/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

class View {

  static getHtmlPath(){
    return '';
  }

  constructor(){
    this.content = undefined
  }

  async loadHtml() {
    const res = await fetch(this.constructor.getHtmlPath())
    if (!res.ok) {
      throw new Error('Couldn\'t load ' + this.htmlPath)
    }
    //We need the html content as dom elements
    var tmp = document.createElement('div');
    tmp.innerHTML = await res.text();
    this.content = tmp.childNodes[0];

    this.onMount()
  }

  onMount(){
    
  }

  getContent(){
    return this.content
  }
}

module.exports = View


/***/ }),
/* 1 */
/***/ (function(module, exports) {


class Player {

  constructor(name) {
    this.name = name;

    this.choiceClickedHandler = () => {}
    
  }

  setGame(game) {
    this.game = game
  }

  askForChoice(done) {
    this.choiceClickedHandler = (choice) => {
      done(choice)
      this.choiceClickedHandler = () => {}
    }
  }

}

module.exports = Player

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

const Application = __webpack_require__(3);

(function () {
  window.onload = () => {
    new Application('#app-container')
  }
})()

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

const Menu  = __webpack_require__(4)
const {Game} = __webpack_require__(5)
const RPSRules = __webpack_require__(8)

class Application {

  static getViews() {

    return {
      menu: {
        component: Menu
      },
      game: {
        component: Game
      }
    }
  }

  constructor(selector) {
    const appContainer = document.querySelector(selector)

    if (!appContainer) {
      throw new Error('App container not found')
    }

    this.appContainer = appContainer

    this.openMenu()
  }

  openMenu() {
    const menu = new Menu();
    
    this.setMainComponent(menu, () => {
      menu.modeSelectedHandler = (mode) => {
        this.openGame(mode)
      }
    })
  }

  openGame(gameMode){
    const game = new Game(gameMode, RPSRules)

    this.setMainComponent(game, () => {
      game.backClickHandler = () => {
        this.openMenu()
      }
    })
  }

  setMainComponent(component, onLoadedCallback){
    component.loadHtml().then(() => {
      const content = component.getContent()
      this.appContainer.innerHTML = '';
      this.appContainer.appendChild(content)
      onLoadedCallback()
    })
  }
 
}

module.exports = Application


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

const View = __webpack_require__(0)

class Menu extends View{

  static getHtmlPath(){
    return './views/menu.html' 
  }

  onMount(){
    this.modeSelectedHandler = () => { }

    this.content.querySelectorAll('.game-mode-btn').forEach(btn => {
      btn.addEventListener('click', () => {
        let gameMode = btn.dataset.mode;

        this.modeSelectedHandler(gameMode)
      })
    })
  }

}

module.exports = Menu


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

const GameRound = __webpack_require__(6)
const Player = __webpack_require__(1)
const CPUPlayer = __webpack_require__(7)
const View = __webpack_require__(0)

const GameMode = {
  PLAYER_VS_CPU: 'player-vs-cpu',
  CPU_VS_CPU: 'cpu-vs-cpu'
}

class Game extends View {

  static getHtmlPath() {
    return './views/game.html'
  }

  constructor(gameMode, rules) {
    super()
    this.gameMode = gameMode

    if (gameMode === GameMode.PLAYER_VS_CPU) {
      this.playerA = new Player('Human')
      this.playerB = new CPUPlayer('Bot')
    } else {
      this.playerA = new CPUPlayer('Bot A')
      this.playerB = new CPUPlayer('Bot B')
    }

    this.rules = rules

    //attach game to players so that they "know" the rules
    this.playerA.setGame(this)
    this.playerB.setGame(this)

    this.isBusy = false

    this.backClickHandler = () => { }

    this.scoreA = 0
    this.scoreB = 0
  }

  onMount() {

    const startButton = this.content.querySelector('#startRound')
    const choiceSelector = this.content.querySelector('.choice-selector')
    const resultText = this.content.querySelector('.resultText')

    this.updateBoard();
    startButton.addEventListener('click', (e) => {
      e.preventDefault()

      if (this.isBusy) {
        //we can't start a round now
        return;
      }

      this.isBusy = true
      console.log('Starting new round')

      let gameRound = new GameRound(this.rules.STRENGTH_MAP, this.playerA, this.playerB)

      gameRound.start((result) => {
        this.isBusy = false
        if (!result.winner) {
          resultText.innerHTML = 'It\'s a tie'
        } else {
          resultText.innerHTML = result.winner.name + ' wins the round!'

          result.winner === this.playerA ? [this.scoreA++, this.scoreB] : [this.scoreA, this.scoreB++]

          this.updateBoard()
        }

        this.showPlayersChoices(result.choiceA, result.choiceB)

        startButton.classList.remove('disabled')
        choiceSelector.classList.remove('active')
      })


      startButton.classList.add('disabled')

      this.clearRound()

      if (this.gameMode === GameMode.PLAYER_VS_CPU) {
        choiceSelector.classList.add('active')

        choiceSelector.querySelectorAll('.choice').forEach((elem) => {
          console.log(elem.dataset)
          elem.addEventListener('click', () => {
            this.playerA.choiceClickedHandler(elem.dataset['choiceKey'])
          })
        })
      }

    })

    this.content.querySelector('#resetScore').addEventListener('click', () => {
      this.scoreA = 0;
      this.scoreB = 0;
      this.updateBoard()
    })
    this.content.querySelector('#backToMenu').addEventListener('click', () => {
       this.backClickHandler() 
      })

  }

  updateBoard() {
    this.content.querySelector('.board .player-a .name').innerHTML = this.playerA.name
    this.content.querySelector('.board .player-b .name').innerHTML = this.playerB.name

    this.content.querySelector('.board .player-a .score').innerHTML = this.scoreA
    this.content.querySelector('.board .player-b .score').innerHTML = this.scoreB
  }

  clearRound() {
    this.content.querySelector('.resultText').innerHTML = 'Waiting for players choices'
    this.content.querySelectorAll('.pin').forEach(elem => elem.classList.remove('chosen'))
  }

  showPlayersChoices(choiceA, choiceB) {

    const choiceAElem = document.querySelector('.choice[data-choice-key="' + choiceA + '"] .pin-a')
    choiceAElem.classList.add('chosen')

    const choiceBElem = document.querySelector('.choice[data-choice-key="' + choiceB + '"] .pin-b')
    choiceBElem.classList.add('chosen')
  }
}

module.exports = {
  Game,
  GameMode
}

/***/ }),
/* 6 */
/***/ (function(module, exports) {

class GameRound{

  constructor(strengthMap, playerA, playerB){
    this.strengthMap = strengthMap

    this.playerA = playerA
    this.playerB = playerB

    this.choiceA = undefined
    this.choiceB = undefined
  }

  setChoiceA(choice){
    this.choiceA = choice
  }

  setChoiceB(choice){
    this.choiceB = choice
  }

  getWinner() {

    const choiceA = this.choiceA
    const choiceB = this.choiceB
    //look for tie
    if(choiceA === choiceB){
      return undefined
    }
    
    if(this.strengthMap[choiceA] === choiceB){
      //a wins
      return this.playerA
    }

    if(this.strengthMap[choiceB] === choiceA){
      //b wins
      return this.playerB
    }

    throw new Error('Invalid strength map')
  }

  checkDone(doneHandler){
    if(this.choiceA && this.choiceB){
      console.log('Round is done')
      doneHandler({
        winner: this.getWinner(),
        choiceA: this.choiceA,
        choiceB: this.choiceB,
      })
    }
  }

  start(doneHandler){
    console.log('Round started')
    this.playerA.askForChoice((choice) => {
      console.log('Player A made his choice')
      this.setChoiceA(choice)
      this.checkDone(doneHandler)
    })
    this.playerB.askForChoice((choice) => {
      console.log('Player B made his choice')
      this.setChoiceB(choice)
      this.checkDone(doneHandler)
    })
  }
}

module.exports = GameRound

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

const Player = __webpack_require__(1);

class CPUPlayer extends Player{

  askForChoice(done){

    window.setTimeout(() => {
    //CPU just pick a choice randomly
    let choicesKeys = Object.keys(this.game.rules.CHOICES)

    const chosenKeyIndex = Math.floor(Math.random() * choicesKeys.length)
    const choice = this.game.rules.CHOICES[choicesKeys[chosenKeyIndex]]


      done(choice)
    }, Math.random() * 100)
  }
}

module.exports = CPUPlayer

/***/ }),
/* 8 */
/***/ (function(module, exports) {


const CHOICES = {
  ROCK: 'rock',
  PAPER: 'paper',
  SCISSORS: 'scissors'
}

const STRENGTH_MAP = {
  [CHOICES.ROCK]: CHOICES.SCISSORS,
  [CHOICES.SCISSORS]: CHOICES.PAPER,
  [CHOICES.PAPER]: CHOICES.ROCK
}

module.exports = {
  CHOICES,
  STRENGTH_MAP
}

/***/ })
/******/ ]);