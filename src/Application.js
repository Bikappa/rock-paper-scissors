const Menu  = require("./views/Menu")
const {Game} = require("./views/Game")
const RPSRules = require("./game/Rules")

class Application {

  static getViews() {

    return {
      menu: {
        component: Menu
      },
      game: {
        component: Game
      }
    }
  }

  constructor(selector) {
    const appContainer = document.querySelector(selector)

    if (!appContainer) {
      throw new Error('App container not found')
    }

    this.appContainer = appContainer

    this.openMenu()
  }

  openMenu() {
    const menu = new Menu();
    
    this.setMainComponent(menu, () => {
      menu.modeSelectedHandler = (mode) => {
        this.openGame(mode)
      }
    })
  }

  openGame(gameMode){
    const game = new Game(gameMode, RPSRules)

    this.setMainComponent(game, () => {
      game.backClickHandler = () => {
        this.openMenu()
      }
    })
  }

  setMainComponent(component, onLoadedCallback){
    component.loadHtml().then(() => {
      const content = component.getContent()
      this.appContainer.innerHTML = '';
      this.appContainer.appendChild(content)
      onLoadedCallback()
    })
  }
 
}

module.exports = Application
