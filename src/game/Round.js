class GameRound{

  constructor(strengthMap, playerA, playerB){
    this.strengthMap = strengthMap

    this.playerA = playerA
    this.playerB = playerB

    this.choiceA = undefined
    this.choiceB = undefined
  }

  setChoiceA(choice){
    this.choiceA = choice
  }

  setChoiceB(choice){
    this.choiceB = choice
  }

  getWinner() {

    const choiceA = this.choiceA
    const choiceB = this.choiceB
    //look for tie
    if(choiceA === choiceB){
      return undefined
    }
    
    if(this.strengthMap[choiceA] === choiceB){
      //a wins
      return this.playerA
    }

    if(this.strengthMap[choiceB] === choiceA){
      //b wins
      return this.playerB
    }

    throw new Error('Invalid strength map')
  }

  checkDone(doneHandler){
    if(this.choiceA && this.choiceB){
      console.log('Round is done')
      doneHandler({
        winner: this.getWinner(),
        choiceA: this.choiceA,
        choiceB: this.choiceB,
      })
    }
  }

  start(doneHandler){
    console.log('Round started')
    this.playerA.askForChoice((choice) => {
      console.log('Player A made his choice')
      this.setChoiceA(choice)
      this.checkDone(doneHandler)
    })
    this.playerB.askForChoice((choice) => {
      console.log('Player B made his choice')
      this.setChoiceB(choice)
      this.checkDone(doneHandler)
    })
  }
}

module.exports = GameRound