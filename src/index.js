const Application = require("./Application");

(function () {
  window.onload = () => {
    new Application('#app-container')
  }
})()