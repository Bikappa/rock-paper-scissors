const Player = require("./Player");

class CPUPlayer extends Player{

  askForChoice(done){

    window.setTimeout(() => {
    //CPU just pick a choice randomly
    let choicesKeys = Object.keys(this.game.rules.CHOICES)

    const chosenKeyIndex = Math.floor(Math.random() * choicesKeys.length)
    const choice = this.game.rules.CHOICES[choicesKeys[chosenKeyIndex]]


      done(choice)
    }, Math.random() * 100)
  }
}

module.exports = CPUPlayer