
class Player {

  constructor(name) {
    this.name = name;

    this.choiceClickedHandler = () => {}
    
  }

  setGame(game) {
    this.game = game
  }

  askForChoice(done) {
    this.choiceClickedHandler = (choice) => {
      done(choice)
      this.choiceClickedHandler = () => {}
    }
  }

}

module.exports = Player