class View {

  static getHtmlPath(){
    return '';
  }

  constructor(){
    this.content = undefined
  }

  async loadHtml() {
    const res = await fetch(this.constructor.getHtmlPath())
    if (!res.ok) {
      throw new Error('Couldn\'t load ' + this.htmlPath)
    }
    //We need the html content as dom elements
    var tmp = document.createElement('div');
    tmp.innerHTML = await res.text();
    this.content = tmp.childNodes[0];

    this.onMount()
  }

  onMount(){
    
  }

  getContent(){
    return this.content
  }
}

module.exports = View
