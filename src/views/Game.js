const GameRound = require("../game/Round")
const Player = require("../player/Player")
const CPUPlayer = require('../player/CPUPlayer')
const View = require("./View")

const GameMode = {
  PLAYER_VS_CPU: 'player-vs-cpu',
  CPU_VS_CPU: 'cpu-vs-cpu'
}

class Game extends View {

  static getHtmlPath() {
    return './views/game.html'
  }

  constructor(gameMode, rules) {
    super()
    this.gameMode = gameMode

    if (gameMode === GameMode.PLAYER_VS_CPU) {
      this.playerA = new Player('Human')
      this.playerB = new CPUPlayer('Bot')
    } else {
      this.playerA = new CPUPlayer('Bot A')
      this.playerB = new CPUPlayer('Bot B')
    }

    this.rules = rules

    //attach game to players so that they "know" the rules
    this.playerA.setGame(this)
    this.playerB.setGame(this)

    this.isBusy = false

    this.backClickHandler = () => { }

    this.scoreA = 0
    this.scoreB = 0
  }

  onMount() {

    const startButton = this.content.querySelector('#startRound')
    const choiceSelector = this.content.querySelector('.choice-selector')
    const resultText = this.content.querySelector('.resultText')

    this.updateBoard();
    startButton.addEventListener('click', (e) => {
      e.preventDefault()

      if (this.isBusy) {
        //we can't start a round now
        return;
      }

      this.isBusy = true
      console.log('Starting new round')

      let gameRound = new GameRound(this.rules.STRENGTH_MAP, this.playerA, this.playerB)

      gameRound.start((result) => {
        this.isBusy = false
        if (!result.winner) {
          resultText.innerHTML = 'It\'s a tie'
        } else {
          resultText.innerHTML = result.winner.name + ' wins the round!'

          result.winner === this.playerA ? [this.scoreA++, this.scoreB] : [this.scoreA, this.scoreB++]

          this.updateBoard()
        }

        this.showPlayersChoices(result.choiceA, result.choiceB)

        startButton.classList.remove('disabled')
        choiceSelector.classList.remove('active')
      })


      startButton.classList.add('disabled')

      this.clearRound()

      if (this.gameMode === GameMode.PLAYER_VS_CPU) {
        choiceSelector.classList.add('active')

        choiceSelector.querySelectorAll('.choice').forEach((elem) => {
          console.log(elem.dataset)
          elem.addEventListener('click', () => {
            this.playerA.choiceClickedHandler(elem.dataset['choiceKey'])
          })
        })
      }

    })

    this.content.querySelector('#resetScore').addEventListener('click', () => {
      this.scoreA = 0;
      this.scoreB = 0;
      this.updateBoard()
    })
    this.content.querySelector('#backToMenu').addEventListener('click', () => {
       this.backClickHandler() 
      })

  }

  updateBoard() {
    this.content.querySelector('.board .player-a .name').innerHTML = this.playerA.name
    this.content.querySelector('.board .player-b .name').innerHTML = this.playerB.name

    this.content.querySelector('.board .player-a .score').innerHTML = this.scoreA
    this.content.querySelector('.board .player-b .score').innerHTML = this.scoreB
  }

  clearRound() {
    this.content.querySelector('.resultText').innerHTML = 'Waiting for players choices'
    this.content.querySelectorAll('.pin').forEach(elem => elem.classList.remove('chosen'))
  }

  showPlayersChoices(choiceA, choiceB) {

    const choiceAElem = document.querySelector('.choice[data-choice-key="' + choiceA + '"] .pin-a')
    choiceAElem.classList.add('chosen')

    const choiceBElem = document.querySelector('.choice[data-choice-key="' + choiceB + '"] .pin-b')
    choiceBElem.classList.add('chosen')
  }
}

module.exports = {
  Game,
  GameMode
}