const View = require('./View')

class Menu extends View{

  static getHtmlPath(){
    return './views/menu.html' 
  }

  onMount(){
    this.modeSelectedHandler = () => { }

    this.content.querySelectorAll('.game-mode-btn').forEach(btn => {
      btn.addEventListener('click', () => {
        let gameMode = btn.dataset.mode;

        this.modeSelectedHandler(gameMode)
      })
    })
  }

}

module.exports = Menu
